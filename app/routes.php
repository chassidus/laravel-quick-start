<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

//http://laravel.com/docs/4.2/quick
Route::get('users', function()
{
   // return 'Users!';
    $env=App::environment();
    var_dump($env);
   // return;
    $users = User::all();

    return View::make('users/users')->with('users', $users);
    
});

//with controller class
Route::get('users2', 'UserController@getIndex');